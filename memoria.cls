%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Clase que provee el formato pedido por el Departamento de Ingeniería
% Informática de la Universidad de Santiago de Chile para las tesis.
%
%
%    This program is free software: you can redistribute it and/or modify
%    it under the terms of the GNU General Public License as published by
%    the Free Software Foundation, either version 3 of the License, or
%    (at your option) any later version.
%
%    This program is distributed in the hope that it will be useful,
%    but WITHOUT ANY WARRANTY; without even the implied warranty of
%    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%    GNU General Public License for more details.
%
%    You should have received a copy of the GNU General Public License
%    along with this program.  If not, see <http://www.gnu.org/licenses/>.
%
%
%       Copyright (C) 2012, 2013, 2014 Felipe Garay
%
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


\ProvidesClass{tesis}[12/03/2015 v16-2 Formato DIINF]


% Para que los márgenes se muevan para imprimir por ambos lados hay que sacar
% oneside
\LoadClass[10pt, letterpaper, oneside]{book}


\usepackage[headheight=2.5cm, top=2.5cm, left=4cm, bottom=2.5cm, right=2.5cm]{geometry}
\usepackage{fancyhdr} %para usar fácilmente los headers
\usepackage{graphicx} %usar gráficos, usado para el logo de la USACH

\usepackage{fontspec}
\setmainfont{Arial}

\newfontfamily\arial{Arial}




%es-lcroman nos permite usar números romanos en minúsculas
\usepackage[spanish, es-lcroman, english]{babel} 
\usepackage[utf8]{inputenc}
\usepackage{tocloft}
\usepackage{etoolbox}
\usepackage{ifthen}


\usepackage{csquotes}
\usepackage{xpatch}
% cargamos biblatex para la bibliografia
\usepackage[citestyle=ieee, style=ieee, backend=biber]{biblatex}
\addbibresource{bibliografia.bib}
\AtEveryBibitem{\clearfield{doi}}
\AtEveryBibitem{\clearfield{month}}
\AtEveryBibitem{\clearfield{timestamp}}
\AtEveryBibitem{\clearfield{biburl}}
\AtEveryBibitem{\clearfield{bibsource}}

%\DefineBibliographyStrings{english}{%
  %urlseen = {visitado el}
%}




\usepackage{url}
\urlstyle{same}


\renewbibmacro*{bbx:savehash}{}

\makeatletter
\newcommand{\customlabel}[2]{%
\protected@write \@auxout {}{\string \newlabel {#1}{{#2}{}}}}
\makeatother


\renewcommand*{\bibfont}{\arial}


% Con esto sabemos si debemos mostrar el nombre del capitulo o no en el header.
% Por ejemplo en los indices no debemos mostrar el capitulo
\newtoggle{mostrarcapitulo}
\togglefalse{mostrarcapitulo}


% Si la pagina es par entonces crea una pagina en blanco, si no entonces
% simplemente salto de pagina. Esto es para que los capítulos siempre empiecen
% en la hoja de la derecha
\newcommand{\paginablanco}{
      \newpage
      %check par, si es par incluir los siguiente
      \ifthenelse{\isodd{\value{page}}}
      {}
      {
              \mbox{}
              \thispagestyle{empty}
              \newpage
      }
}


% Genera la portada del informe
% Argumentos: {Titulo}{Autores}{Profesora \\ ayudantes}{fecha}
\newcommand{\portada}[4]{

        \pagestyle{fancy}
        \fancyhf{} %Sin estilo de pagina

        \renewcommand{\headrulewidth}{0pt} %sacamos la linea horizontal del header

        % header central
        \chead{
                \fontsize{14}{17}
                \arial
                \selectfont
                \linespread{1.2}
                \textbf{UNIVERSIDAD DE SANTIAGO DE CHILE} \\
                \fontsize{12}{14}
                \selectfont
                \textbf{FACULTAD DE INGENIERÍA} \\
                \textbf{DEPARTAMENTO DE INGENIERÍA INFORMÁTICA}}

        % logo universidad
    \rhead{\vspace*{0.8cm}\includegraphics[height=2cm]{usach.png}}

		\vspace*{0.5cm}
        % titulo
        \begin{center}
                \fontsize{14}{17}
                \arial
                \selectfont
                \textbf{\uppercase{#1}} \\	% debe estar en mayusculas
        \end{center}


        % autores
        \begin{center}
                \fontsize{10}{11}
                \arial
                \selectfont
                \textbf{\uppercase{#2}}		% debe estar en mayusculas
        \end{center}

        % el profesor debe esta a la mitad, con vfill llenamos con espacios
        % hasta la mitad
        \vfill

        %profesor guia, ayudantes
        \fontsize{10}{11}
        \arial
        \selectfont
        \hskip 6.795cm % debe estar desde la mitad hacia la derecha
        \begin{minipage}[b]{0.45\linewidth}
                #3
        \end{minipage}


        % con mas espacios hasta el final de la pagina
        \vfill


        % fecha y lugar
        \begin{center}
                \fontsize{10}{11}
                \arial
                \selectfont
                Santiago - Chile \\
                #4
        \end{center}

        \paginablanco

        \pagestyle{fancy}
        \fancyhf{} %reset el estilo


        \renewcommand{\headrulewidth}{0.4pt} 

        \newcommand{\nombrecaptitulo}{}

        \fancyhead[LE,RO]{\thepage}
        \fancyhead[RE,LO]{\capituloheading}
        
        \fancyfoot[F]{}
}


% Crea los indices del contenido, figuras y tablas
\newcommand{\indices}{
        %\pagenumbering{roman}

        \newpage
        \arial

        \renewcommand{\thepage}{\roman{page}} % numeración romana

        % para que los capitulos tengan puntos entre el nombre y la pagina
        \renewcommand{\cftchapdotsep}{\cftdot}

        \setcounter{tocdepth}{3}

        \renewcommand{\contentsname}{\arial \textbf{TABLA DE CONTENIDOS}}
        \tableofcontents
        \thispagestyle{fancy}

        \newpage

        \renewcommand{\cftfigdotsep}{\cftdotsep}

        \renewcommand*\listfigurename{\arial \textbf{ÍNDICE DE FIGURAS}}
        \addcontentsline{toc}{chapter}{ÍNDICE DE FIGURAS}
        \listoffigures
        \thispagestyle{fancy}

        \newpage


        \renewcommand{\cfttabdotsep}{\cftdotsep}

        \renewcommand*\listtablename{\arial \textbf{ÍNDICE DE CUADROS}}
        \addcontentsline{toc}{chapter}{ÍNDICE DE CUADROS}
        \listoftables

        \thispagestyle{fancy}
        

        \newpage

        \renewcommand{\thepage}{\arabic{page}} % Numeración arábiga
        %\pagenumbering{arabic}

}



% ve si es necesario mostrar el capitulo en el heading.
\newcommand{\capituloheading}{
        \iftoggle{mostrarcapitulo}{
                \arial CAPÍTULO \arabic{chapter}: \nombrecaptitulo
        }{}
}


% Genera un capitulo, equivalente a \chapter
% Argumentos: {Titulo del capitulo}
\newcommand{\capitulo}[1]{

        \paginablanco
        
        \fontsize{16}{19}
        \arial
        \selectfont

        \addtocounter{chapter}{1}

        \noindent\textbf{CAPÍTULO \arabic{chapter}.  \hspace{0.2cm} \uppercase{#1}} \newline
        

        \addcontentsline{toc}{chapter}{\arial CAPÍTULO \arabic{chapter}. \hspace{0.2cm} \uppercase{#1}}

        \renewcommand{\nombrecaptitulo}{\uppercase{#1}}

        \thispagestyle{empty}



        % para que muestre el nombre del capitulo en el heading
        \toggletrue{mostrarcapitulo}

        %resetamos los counters
        \setcounter{figure}{0}
        \setcounter{table}{0}
        \setcounter{section}{0}
        \setcounter{equation}{0}
        \setcounter{subsection}{0}


        % fuente estándar para el texto
        \fontsize{10}{11}
        \arial
        \selectfont
}

% crea una sección, equivalente a \seccion
% Argumentos: {Titulo de la sección}
\newcommand{\seccion}[1]{

        \fontsize{14}{17}
        \arial
        \selectfont
        
        \addtocounter{section}{1}
        \setcounter{subsection}{0}

        \noindent\textbf{\arabic{chapter}.\arabic{section} \hspace{0.2cm}  \uppercase{#1}}
        \vspace{5mm}

        \addcontentsline{toc}{section}{\arial \arabic{chapter}.\arabic{section} \hspace{0.5cm}\uppercase{#1}}

        \fontsize{10}{11}
        \arial
        \selectfont

}



% crea una subseccion, equivalente a \subseccion
% Argumentos: {Titulo de la subseccion}
\newcommand{\subseccion}[1]{

        \fontsize{12}{14}
        \arial
        \selectfont
        
        \addtocounter{subsection}{1}

        \textbf{\arabic{chapter}.\arabic{section}.\arabic{subsection} \hspace{0.2cm} #1}
        \vspace{3mm}

        \addcontentsline{toc}{subsection}{\arial \arabic{chapter}.\arabic{section}.\arabic{subsection} \hspace{0.2cm} #1}


        \fontsize{10}{11}
        \arial
        \selectfont

}


% crea una subsubseccion, equivalente a \subsubseccion
% Argumentos: {Titulo de la subsubseccion}
\newcommand{\subsubseccion}[1]{

        \fontsize{12}{14}
        \arial
        \selectfont
        
        \addtocounter{subsubsection}{1}

        \emph{#1}

        \addcontentsline{toc}{subsubsection}{\arial #1}

        \fontsize{10}{11}
        \arial
        \selectfont

}



% Define una tabla
% Argumentos {caption}{tabla}{label}
\newcommand{\tabla}[3]{
        \fontsize{12}{14}
        \arial
        \selectfont

        \addtocounter{table}{1}


        \begin{center}
                \emph{Tabla \arabic{chapter}.\arabic{table}: #1} \\
                        #2
        \end{center}

        \addcontentsline{lot}{table}{\arial Tabla \arabic{chapter}.\arabic{table}: #1} 
        \customlabel{#3}{\arial Tabla \arabic{chapter}-\arabic{table}}

        \fontsize{10}{11}
        \arial
        \selectfont
}


% Define una figura
% Argumentos {CAPTION}{FIGURA}{label}
\newcommand{\figura}[3]{
  \addtocounter{figure}{1}

  \begin{center}
    {\makebox[\textwidth][c]{#2}}
          \emph{Figura \arabic{chapter}-\arabic{figure}: #1} \\
  \end{center}

  \addcontentsline{lof}{figure}{\arial Figura \arabic{chapter}-\arabic{figure}: #1}

  \customlabel{#3}{\arial Figura \arabic{chapter}-\arabic{figure}}
}


% crea una bibliografia con el estilo apa y la base de datos bibliografia.bib
\newcommand{\bibliografia}{
  \capitulo{Bibliografía}
  { \arial
    \printbibliography[heading=none]
  }
}


\newcommand{\bibliografiasincita}{
  \capitulo{Bibliografía}
  { \arial
    \printbibliography[heading=none]
  }
  \nocite{*}
}

\renewcommand{\baselinestretch}{1.5} %interlineado 1.5


% Genera el título adecuado para poner un resumen
% Argumentos: {título}
% Ejemplo
%   \resumen{Resumen}
%   \resumen{Abstract}
\newcommand{\resumen}[1]{
        \renewcommand{\thepage}{\roman{page}} % numeración romana
        \thispagestyle{fancy}

        \paginablanco
        
        \fontsize{16}{19}
        \arial
        \selectfont


        \noindent\textbf{\uppercase{#1}}
        

        \addcontentsline{toc}{chapter}{\arial \uppercase{#1}}


        % fuente estándar para el texto
        \fontsize{10}{11}
        \arial
        \selectfont
}



