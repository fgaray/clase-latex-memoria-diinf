# Clase LaTeX para el formato memoria/tesis del Departamento de Informática, USACH

Esta clase agrega el formato de memoria/tesis a LaTeX. Cumple con el checklist
de evaluación a la fecha del segundo semestre de 2016.


Gracias a los contribuidores que han proporcionado mejoras y correcciones:

* Felipe Garay
* Patricio Jara
* Ian Mejías

## Requisitos

* latex
* xelatex
* biber
* ttf-mscorefonts-installer



# Uso de la clase

*NOTA: Esta sección no pretende ser un tutorial de LaTeX*

Para usar esta clase necesitas tener instalado pdflatex. Es recomendable que se
instalen todos los paquetes de la suite latex.


## Compilación

La compilación se puede realizar por medio de latexmk.


## Agregando la tesis a un documento

Puede utilizar el archivo prueba.tex cómo base para sus documentos. En otros
archivos debe incluir en el preámbulo lo siguiente:

    \documentclass{tesis}

Ese comando va a agregar todo lo necesario para que el documento generado
respete el formato de memoria.


## Portada

La portada se genera sola con el comando \portada. Aquí hay un ejemplo de uso:

    \portada{TITULO}{AUTOR}{PROFESOR, AYUDANTES}{FECHA}


## Indices

El comando \indices genera los indices de: capítulos, tablas y figuras. Se
utiliza simplemente así:

    \indices



## Capítulos

Los capítulos son generados por el comando \capitulo:

    \capitulo{TITULO CAPITULO}

LaTeX solo capitaliza el titulo y se encarga de los headers.


### Sección, Subsección y Subsubsección

Estos comandos crean secciones dentro de un capitulo. Se utilizan igual que el
comando capitulo. Es responsabilidad del usuario respetar el orden de
profundidad:


    \capitulo{Capitulo 1}

    \seccion{Seccion 1}

    \seccion{Seccion 2}

    \subseccion{Subseccion 2.1}


## Tablas

El comando \tabla tiene el siguiente formato:

    \tabla{Titulo Tabla}{Comandos que generan la tabla}{Etiqueta}


Los comandos que generan la tabla deben estar dentro de un entorno tabular o
tabularx.


## Figuras

El comando para las figuras es el siguiente:

    \figura{TITULO}
        {\includegraphics[width=15cm]{ARCHIVO}}
        {ETIQUETA}

El ancho de 15cm queda bien en el formato de memoria pero puede ser cambiado
según las necesidades.



## Formatos no considerados

El formato de memoria no especifica un formato para los algoritmos así que no se
incluye uno en esta clase.



# Desarrollo

El desarrollo principal de este repositorio se encuentra en

[https://bitbucket.org/fgaray/clase-latex-memoria-diinf](https://bitbucket.org/fgaray/clase-latex-memoria-diinf)

Si desea contribuir con nuevas características o correcciones de errores, debe tener una cuenta
en bitbucket.org, hacer un fork (copia) del repositorio, realizar sus
modificaciones y luego hacer un "Pull Request" para que sus cambios sean
añadidos al repositorio principal.

Más información puede ser encontrada en el manual de bitbucket:

* [Fork](https://confluence.atlassian.com/display/BITBUCKET/Forking+a+Repository)
* [Pull Request](https://confluence.atlassian.com/display/BITBUCKET/Working+with+pull+requests)

## Commits

En [esta](http://tbaggery.com/2008/04/19/a-note-about-git-commit-messages.html)
pagina hay una buena guia sobre cómo escribir buenos commits y debería ser
respetada al momento de contribuir con código.

# Créditos

    Felipe Garay: Código original.

